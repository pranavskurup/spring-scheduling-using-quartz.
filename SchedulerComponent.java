import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

@Component
public class SchedulerComponent {

	Job realTimeJob;

	SchedulerFactoryBean scheduler;
	@Autowired
	public SchedulerComponent(Job realTimeJob, SchedulerFactoryBean scheduler) throws SchedulerException {
		super();
		this.realTimeJob = realTimeJob;
		this.scheduler=scheduler;
		schedulejobTask();
	}

	private void schedulejobTask() throws SchedulerException {
		JobDetail job = JobBuilder.newJob(RealTimeJob.class)
				.withIdentity("realTimeJob", "group").build();

		String expression = "5 * * * * ?";
		Trigger trigger = TriggerBuilder.newTrigger()
				.withIdentity("realTimeJob", "group")
				.withSchedule(CronScheduleBuilder.cronSchedule(expression))
				.build();

		scheduler.getScheduler().scheduleJob(job, trigger);
		scheduler.start();
	}

}
