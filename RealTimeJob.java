import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("realTimeJob")
public class RealTimeJob implements Job {

	@Autowired
	MyService myService;

	public void execute(JobExecutionContext _context)
			throws JobExecutionException {

		System.out.println(myService.doGetSomeString());

	}
}